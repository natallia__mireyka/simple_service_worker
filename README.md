It`s a simple example of Service Worker offline caching functionality.

To use it install the LiveServer extension in VSCode and click 'open with LiveServer' so it will open http://127.0.0.1:5500/index.html by default.

You can change used Service Worker file in main.js from sw_cached_site.js to sw_cached_pages.js to see the different cache strategies.
